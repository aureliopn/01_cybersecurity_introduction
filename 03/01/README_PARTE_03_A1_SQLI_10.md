# Introducción a la Ciberseguridad - Vulnerabilidad A1 SQL Injection (Apartado 10)

Conociendo que la aplicación construye una query dinámica concatenando una variable numérica de la siguiente manera:

```java
String sql = "SELECT * FROM user_data WHERE login_count = " + Login_Count + " AND userid = "  + User_ID;
```

Se puede explotar la vulnerabilidad pasando en el segundo parámetro del formulario el texto "1 OR true" para que la consulta resultante sea la siguiente:

```sql
SELECT * FROM user_data WHERE login_count = 2134 AND userid= 1 OR true;
```

![SQL Injection](./img/01-a1_sqli_10.gif)
