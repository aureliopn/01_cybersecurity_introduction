# Introducción a la Ciberseguridad - Vulnerabilidad A5 Insecure Direct Object References (Apartado 3)

En este caso la aplicación está enviando al cliente más información de la necesaria. Cuando se realiza la petición, el cliente muestra la siguiente información al usuario:

```json
{
  "color" : "yellow",
  "size" : "small",
  "name" : "Tom Cat"
}
```

 Sin embargo, si se inspecciona la respuesta de la petición se puede observar la información adicional que no se está mostrando en la parte cliente, pero que sí se está recibiendo del servidor:

```json
{
  "role" : 3,
  "color" : "yellow",
  "size" : "small",
  "name" : "Tom Cat",
  "userId" : "2342384"
}
```

![Insecure Direct Object References](./img/01-a5_idor_03.gif)
