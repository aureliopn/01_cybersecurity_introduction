# Introducción a la Ciberseguridad - Vulnerabilidad A5 Insecure Direct Object References (Apartado 4)

Con el uso de la aplicación se puede intuir que utiliza un patrón [RESTfull](https://en.wikipedia.org/wiki/Representational_state_transfer). De esta manera se puede "*adivinar*" una manera alternativa para obtener la información del perfil del usuario utilizando el endpoint *http://127.0.0.1:8080/WebGoat/IDOR/profile/{userId}*:

![Insecure Direct Object References](./img/01-a5_idor_04.gif)
