# Introducción a la Ciberseguridad - Vulnerabilidad A5 Missing Function Level Access Control (Apartado 2)

Pueden existir elementos en la vista de la aplicación que están ocultos, pero que son visibles si se inspecciona el código fuente de la parte cliente. En este ejercicio se han encontrado dos elementos del menú ocultos que pueden dar más información de la necesaria a un potencial usuario malicioso:

![Missing Function Level Access Control](./img/01-a5_mflac_02.gif)
