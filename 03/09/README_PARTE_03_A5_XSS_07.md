# Introducción a la Ciberseguridad - Vulnerabilidad A7 Cross Site Scripting (Apartado 7)

En el formulario hay información que no está validando el servidor y que se está incluyendo en la respuesta al cliente. Se puede comprobar introduciendo el texto ```<script>alert()</script>``` en el campo de la tarjeta de crédito:

![Cross Site Scripting](./img/01-a5_xss_01.gif)

Esto demuestra la existencia de una vulnerabilidad ante un ataque *Reflected XSS*.
