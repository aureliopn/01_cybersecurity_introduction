# Introducción a la Ciberseguridad - Informe de Auditoría

* Informe:
  * Auditoría sobre la aplicación web "WebGoat".
* Localización:
  * Madrid.
* Autores:
  * Aurelio Pérez Noriega.
* Versiones:
  * 07/12/2021 - v1.0.
* Documento PDF:
  * [Auditoria_Seguridad.pdf](./Auditoria_Seguridad.pdf).

## Ámbito y alcance

Esta auditoría tiene un ámbito académico, y el alcance está determinado por los puntos descritos en el [enunciado](../Enunciado_Practica_Introduccion_Ciberseguridad.pdf) de la práctica.

La auditoría se centra en la aplicación web "WebGoat", utilizando una aproximación [*White-Box*](https://en.wikipedia.org/wiki/White-box_testing), con acceso directo a:

* La aplicación.
* Las partes internas del código fuente.

## Informe ejecutivo

### Resumen del proceso realizado

Durante la auditoría se han realizado las siguientes operaciones:

* Levantar el entorno con la aplicación a estudiar.
* Information gathering (reconocimiento) de la información relativa a la aplicación.
* Detectar y explotar vulnerabilidades.
* Elaborar el informe de la auditoría.

### Vulnerabilidades destacadas

Se han encontrado las siguientes vulnerabilidades en la aplicación, con sus consecuencias potenciales:

* [SQL Injection](https://en.wikipedia.org/wiki/SQL_injection):
  * Lectura y modificación de información sensible de la base de datos.
  * Ejecución de operaciones administrativas en la base de datos.
    * Inhabilitación de auditoría.
    * Truncado de tablas y logs.
    * Creación de usuarios.
  * Obtención de información de los archivos existentes en el sistema de ficheros en el que se encuentra la base de datos.
  * Ejecutar comandos del sistema operativo.
* [Insecure Direct Object References](https://en.wikipedia.org/wiki/Insecure_direct_object_reference):
  * Acceso indebido a información por parte de un usuario sin suficientes privilegios.
  * Ejecución de operativas por parte de un usuario sin suficientes privilegios.
* [Missing Function Level Access Control](https://blog.detectify.com/2016/07/13/owasp-top-10-missing-function-level-access-control-7/):
  * Ejecución de partes de la aplicación sin control de acceso a usuarios maliciosos.
* [Cross Site Scripting (XSS)](https://en.wikipedia.org/wiki/Cross-site_scripting):
  * Robo de la *cookie* de la sesión de un usuario.
  * Creación de falsas peticiones.
  * Creación de campos falsos en formularios con el fin de obtener credenciales.
  * Redirección a sitios web peligrosos.
  * Robo de información confidencial.
  * Ejecución de código malicioso en el sistema del usuario final.
  * Inserción de contenido hostil o malicioso.
  * Dar apoyo a ataques *phishing* (al utilizar un dominio válido en la URL).

### Conclusiones

La aplicación tiene vulnerabilidades de riesgo alto según [Common Vulnerability Scoring System Calculator](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator):

* SQL Injection 9.9:
  * Autenticación requerida.
  * Confidencialidad de la información comprometida.
  * Integridad de la información comprometida.
  * Disponibilidad de la información comprometida.
  * Baja complejidad de acceso, no se requieren grandes habilidades para atacar al sistema.

![SQL Injection CVSSC](./img/01-sqli-cvssc.jpg)

* Insecure Direct Object References 9.6:
  * Autenticación requerida.
  * Confidencialidad de la información comprometida.
  * Integridad de la información comprometida.
  * Baja complejidad de acceso, no se requieren grandes habilidades para atacar al sistema.

![Insecure Direct Object References CVSSC](./img/02-idor-cvssc.jpg)

* Missing Function Level Access Control 9.6:
  * Autenticación requerida.
  * Confidencialidad de la información comprometida.
  * Integridad de la información comprometida.
  * Baja complejidad de acceso, no se requieren grandes habilidades para atacar al sistema.

![Missing Function Level Access Control CVSSC](./img/03-mflac-cvssc.jpg)

* Cross Site Scripting (XSS) 2.8:
  * Autenticación no requerida (por el atacante).

![Cross Site Scripting (XSS) CVSSC](./img/04-xss-cvssc.jpg)

### Recomendaciones

Se recomienda:

* Realizar las correcciones oportunas para mitigar las vulnerabilidades descubiertas.
* Incorporar en el equipo de desarrollo las pautas de programación que prevengan las vulnerabilidades de seguridad.
* Incorporar en el equipo de DevOps los tests automáticos oportunos para detectar las vulnerabilidades antes de que lleguen a un entorno productivo.

## Descripción del proceso

### Information gathering (reconocimiento)

En este apartado está toda la información relativa al [Information gathering (reconocimiento)](../02/README_PARTE_02_RECONOCIMIENTO.md) de la aplicación a auditar.

### Explotación de vulnerabilidades detectadas

En este apartado está toda la información relativa a la explotación de las vulnerabilidades detectadas:

* [A1 SQL Injection - Apartado 10](../03/01/README_PARTE_03_A1_SQLI_10.md).
* [A1 SQL Injection - Apartado 11](../03/02/README_PARTE_03_A1_SQLI_11.md).
* [A1 SQL Injection - Obtención de información](../03/03/README_PARTE_03_A1_SQLI_OBTENCION_INFORMACION.md).
* [A5 Insecure Direct Object References - Apartado 3](../03/04/README_PARTE_03_A5_IDOR_03.md).
* [A5 Insecure Direct Object References - Apartado 4](../03/05/README_PARTE_03_A5_IDOR_04.md).
* [A5 Insecure Direct Object References - Apartado 5](../03/06/README_PARTE_03_A5_IDOR_05.md).
* [A5 Missing Function Level Access Control - Apartado 2](../03/07/README_PARTE_03_A5_MFLAC_02.md).
* [A5 Missing Function Level Access Control - Apartado 3](../03/08/README_PARTE_03_A5_MFLAC_03.md).
* [A7 Cross Site Scripting (XSS) - Apartado 7](../03/09/README_PARTE_03_A5_XSS_07.md).

### Post-explotación

No se ha conseguido obtener un acceso para la ejecución remota de código ([RCE](https://encyclopedia.kaspersky.com/glossary/remote-code-execution-rce/)).

### Posibles mitigaciones

Recomendaciones ante las vulnerabilidades encontradas:

* SQL Injection:
  * *Static Queries*.
  * *Parameterized Queries*.
  * *Stored Procedures* (si no generan SQL dinámico).
* Insecure Direct Object References:
  * [JWT](https://en.wikipedia.org/wiki/JSON_Web_Token) para la autenticación de las peticiones de los usuarios en el servidor.
  * Filtrado del acceso a las operativas de la aplicación (siempre en el servidor) en función de los *claims* del usuario.
* Missing Function Level Access Control:
  * Denegar acceso a todos los recursos de la aplicación por defecto, incluso a las partes de URLs o APIs que no son públicas.
* Cross Site Scripting (XSS):
  * El servidor de la aplicación debe validar toda la información que recibe del cliente. Esta vulnerabilidad se produce porque el servidor envía en su respuesta la información recibida desde el cliente sin realizar validación.

### Herramientas utilizadas

Se han utilizado los siguientes sistemas operativos y herramientas:

* [Windows](https://www.microsoft.com/es-es/software-download/windows10) 10.
  * [VirtualBox](https://www.virtualbox.org/) 6.1.12 r139181 (Qt5.6.2).
* [Kali Linux](https://www.kali.org/get-kali/#kali-virtual-machines) 2021.3-virtualbox-amd64.
  * [Docker](https://docs.docker.com/install/linux/docker-ce/debian/) 20.10.8+dfsg1.
  * [WebGoat](https://hub.docker.com/r/webgoat/goatandwolf) v8.1.0.
  * [Nmap](https://nmap.org) 7.91.
  * [Burp Suite](https://portswigger.net/burp) Community Edition v2021.8.2 Build 9403.
  * [Wappalyzer](https://chrome.google.com/webstore/detail/wappalyzer/gppongmhjkpfnbhagpmjfkannfbllamg/)
  * [Sqlmap](https://sqlmap.org/) 1.5.8.
  * [Git](https://www.git-scm.com/) 2.32.0.
  * [Peek](https://github.com/phw/peek) 1.5.1.
