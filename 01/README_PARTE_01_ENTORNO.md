# Introducción a la Ciberseguridad - Levantar Entorno

Se utilizará la aplicación web WebGoat versión 8.1.0.

## Levantar aplicación con Docker

Se puede levantar la aplicación ejecutando:

```bash
docker run -p 8080:8080 -p 9090:9090 -e TZ=Europe/Amsterdam webgoat/goatandwolf:v8.1.0
```

![WebGoat Run](./img/01-webgoat_run.gif)

## Acceder a la aplicación

La aplicación será accesible en la dirección [http://127.0.0.1:8080/WebGoat/login](http://127.0.0.1:8080/WebGoat/login):

![WebGoat Login](./img/02-webgoat_login.jpg)
