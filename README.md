# Introducción a la Ciberseguridad - Bootcamp Cybersecurity III

Proyecto académico con el objetivo de realizar una auditoría web básica ([enunciado](./Enunciado_Practica_Introduccion_Ciberseguridad.pdf)).

## Herramientas

Se han utilizado los siguientes sistemas operativos y herramientas en la realización de los ejemplos:

* [Windows](https://www.microsoft.com/es-es/software-download/windows10) 10.
  * [VirtualBox](https://www.virtualbox.org/) 6.1.12 r139181 (Qt5.6.2).
* [Kali Linux](https://www.kali.org/get-kali/#kali-virtual-machines) 2021.3-virtualbox-amd64.
  * [Docker](https://docs.docker.com/install/linux/docker-ce/debian/) 20.10.8+dfsg1.
  * [WebGoat](https://hub.docker.com/r/webgoat/goatandwolf) v8.1.0.
  * [Nmap](https://nmap.org) 7.91.
  * [Burp Suite](https://portswigger.net/burp) Community Edition v2021.8.2 Build 9403.
  * [Wappalyzer](https://chrome.google.com/webstore/detail/wappalyzer/gppongmhjkpfnbhagpmjfkannfbllamg/)
  * [Sqlmap](https://sqlmap.org/) 1.5.8.
  * [Git](https://www.git-scm.com/) 2.32.0.
  * [Peek](https://github.com/phw/peek) 1.5.1.

## Contenido

* [Primera parte, levantar entorno](./01/README_PARTE_01_ENTORNO.md).
* [Segunda parte, information gathering (reconocimiento)](./02/README_PARTE_02_RECONOCIMIENTO.md).
* Tercera parte, detección y explotación de vulnerabilidades:
  * [A1 SQL Injection - Apartado 10](./03/01/README_PARTE_03_A1_SQLI_10.md).
  * [A1 SQL Injection - Apartado 11](./03/02/README_PARTE_03_A1_SQLI_11.md).
  * [A1 SQL Injection - Obtención de información](./03/03/README_PARTE_03_A1_SQLI_OBTENCION_INFORMACION.md).
  * [A5 Insecure Direct Object References - Apartado 3](./03/04/README_PARTE_03_A5_IDOR_03.md).
  * [A5 Insecure Direct Object References - Apartado 4](./03/05/README_PARTE_03_A5_IDOR_04.md).
  * [A5 Insecure Direct Object References - Apartado 5](./03/06/README_PARTE_03_A5_IDOR_05.md).
  * [A5 Missing Function Level Access Control - Apartado 2](./03/07/README_PARTE_03_A5_MFLAC_02.md).
  * [A5 Missing Function Level Access Control - Apartado 3](./03/08/README_PARTE_03_A5_MFLAC_03.md).
  * [A7 Cross Site Scripting (XSS) - Apartado 7](./03/09/README_PARTE_03_A5_XSS_07.md).
* [Cuarta parte, informe de auditoría](./04/README_PARTE_04_INFORME_AUDITORIA.md).

## Corrección del profesor

El resultado de tu práctica es APTO.

Lo primero, aun que ya lo hablamos por Slack, es necesario entregar un informe. No se suele admitir un repositorio como "informe".

Dicho esto entiendo que fue por falta de tiempo, tal como me comentaste, y la verdad es que los archivos md están muy bien documentados. Se ha incluido un detalle de cada vulnerabilidad y proceso muy bien documentado. Te hubiera faltado juntar toda esta documentación y darle forma en un informe.

Este posible informe además es recomendable que incluya una sección de informe ejecutivo. Y en cada vulnerabilidad el cliente va a esperar recomendaciones o posibles soluciones a la misma. Cada vulnerabilidad detectada debería incluir una descripción y links asociados a posibles soluciones.

El PDF está, pero al final supongo que es un md a PDF que has maquetado un poco. Profesionalmente te tendría que quedar algo como [esto](https://protonmail.com/blog/wp-content/uploads/2021/07/securitum-protonmail-security-audit.pdf).

De todas formas es tu primer informe, muy buen trabajo. Y yo como informático aprecio el md y todos los GIFs. Esta muy explicado.
