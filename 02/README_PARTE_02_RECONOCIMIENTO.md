# Introducción a la Ciberseguridad - Information Gathering (Reconocimiento)

Obtener la máxima información posible sobre la aplicación.

## Puertos abiertos

Para levantar el entorno se está utilizando Docker, y del contendor que ejecuta la aplicación se están exponiendo 2 puertos:

* 8080: WebGoat.
* 9090: WebWolf.

Esto lo sabemos por la instrucción que hemos ejecutado para levantar el entorno:

```bash
docker run -p 8080:8080 -p 9090:9090 -e TZ=Europe/Amsterdam webgoat/goatandwolf:v8.1.0
```

En esta práctica nuestro caso de estudio se centra en la aplicación expuesta en el puerto 8080 (WebGoat). De todas maneras, podemos comprobar que WebWolf está levantado en el puerto 9090:

![WebWolf Run](./img/01-webwolf_port_9090.jpg)

Información obtenida con Nmap de nuestra máquina local:

* Puerto 8080: servicio http-proxy.
* Puerto 8081: blackice-icecap (está fuera de nuestro entorno de pruebas).
* Puerto 9090: servicio zeus-admin.

```bash
sudo nmap -O 127.0.0.1
```

![Nmap](./img/02-nmap_localhost.jpg)

Información obtenida con Nmap para el puerto 8080:

* Información de versión del servicio http-proxy desconocida.

```bash
nmap -sV -p 8080 127.0.0.1
```

![Nmap 8080](./img/02-nmap_port_8080.jpg)

Información obtenida con Nmap para el puerto 9090:

* Información de versión del servicio zeus-admin desconocida.

```bash
nmap -sV -p 9090 127.0.0.1
```

![Nmap 9090](./img/02-nmap_port_9090.jpg)

## Sistema operativo

Al utilizar un entorno levantado con Docker la IP del contenedor es la misma que en el host. En esta fase no es posible averiguar la versión del sistema operativo que se ejecuta dentro del contenedor utilizando Nmap, porque el contenedor no expone puertos que faciliten esa información.

## Lenguajes de programación utilizados en la web

Utilizando la extensión Wappalyzer para el navegador web se puede obtener la siguiente información acerca de las tecnologías utilizadas:

* JavaScript frameworks:
  * [RequireJS](https://requirejs.org/) 2.3.6.
  * [Backbone.js](https://backbonejs.org/) 1.4.0.
* Font scripts:
  * [Font Awesome](https://fontawesome.com/).
* Programming languages:
  * [Java](https://java.com/).
* JavaScript libraries:
  * [Underscore.js](http://underscorejs.org/).
  * [jQuery UI](https://jqueryui.com/) 1.10.4.
  * [jQuery](https://jquery.com/) 2.1.4.
* UI frameworks.
  * [Bootstrap](https://getbootstrap.com/) 3.3.7.
  * [animate.css](https://animate.style/).

![WebGoat Wappalyzer](./img/03-webgoat_wappalyzer.gif)
